#ifndef SNIGL_TERM_KEY_H
#define SNIGL_TERM_KEY_H

#include "snigl/int.h"

#define term_key_new(...)                         \
  _term_key_new((unsigned char[]){__VA_ARGS__, 0})  \
  
extern sgl_int_t TERM_KEY_BSPACE, TERM_KEY_DOWN, TERM_KEY_ESC,
  TERM_KEY_F1, TERM_KEY_F2, TERM_KEY_F3, TERM_KEY_F4, TERM_KEY_F5, TERM_KEY_F6,
  TERM_KEY_F7, TERM_KEY_F8, TERM_KEY_F9, TERM_KEY_F10,
  TERM_KEY_LEFT, TERM_KEY_RIGHT, TERM_KEY_RETURN,
  TERM_KEY_UP;

sgl_int_t _term_key_new(unsigned char in[]);

void term_init_keys();

#endif
