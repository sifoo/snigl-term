#ifndef SNIGL_TERM_LIB_H
#define SNIGL_TERM_LIB_H

#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/pool.h"
#include "snigl/val.h"

struct term_lib {
  struct sgl_lib lib;
  struct sgl_type *Key;
  struct sgl_val k_bspace, k_down, k_esc, k_f1, k_f2, k_left, k_return, k_right, k_up;
};

struct sgl_lib *term_lib_new(struct sgl *sgl, struct sgl_pos *pos);
bool snigl_term_init(struct sgl *sgl, struct sgl_pos *pos);

#endif
