#include <ctype.h>

#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/int.h"
#include "snigl/val.h"

#include "snigl_term/key.h"
#include "snigl_term/lib.h"
#include "snigl_term/types/key.h"

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_int_t k = val->as_int;
  
  if (k == TERM_KEY_BSPACE) {
    sgl_buf_putcs(out, "k-bspace");
  } else if (k == TERM_KEY_DOWN) {
    sgl_buf_putcs(out, "k-down");
  } else if (k == TERM_KEY_ESC) {
    sgl_buf_putcs(out, "k-esc");
  } else if (k == TERM_KEY_LEFT) {
    sgl_buf_putcs(out, "k-left");
  } else if (k == TERM_KEY_RIGHT) {
    sgl_buf_putcs(out, "k-right");
  } else if (k == TERM_KEY_UP) {
    sgl_buf_putcs(out, "k-up");
  } else if (k == TERM_KEY_F1) {
    sgl_buf_putcs(out, "k-f1");
  } else if (k == TERM_KEY_F2) {
    sgl_buf_putcs(out, "k-f2");
  } else if (isgraph(k)) {
    sgl_buf_printf(out, "#%c", k);
  } else {
    sgl_buf_printf(out, "\\%" SGL_INT, k);
  }
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_buf_putc(out, val->as_int);
}

struct sgl_type *term_key_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_int_type_new(sgl, pos, lib, id, parents);
  t->dump_val = dump_val;
  t->print_val = print_val;
  return t;
}
