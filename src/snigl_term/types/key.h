#ifndef SNIGL_TERM_TYPES_KEY_H
#define SNIGL_TERM_TYPES_KEY_H

struct sgl_lib;
struct sgl_sym;
struct sgl_type;

struct sgl_type *term_key_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]);

#endif
