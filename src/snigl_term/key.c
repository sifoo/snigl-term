#include "snigl_term/key.h"

sgl_int_t TERM_KEY_BSPACE, TERM_KEY_DOWN, TERM_KEY_ESC,
  TERM_KEY_F1, TERM_KEY_F2, TERM_KEY_F3, TERM_KEY_F4, TERM_KEY_F5, TERM_KEY_F6,
  TERM_KEY_F7, TERM_KEY_F8, TERM_KEY_F9, TERM_KEY_F10,
  TERM_KEY_LEFT, TERM_KEY_RIGHT, TERM_KEY_RETURN, TERM_KEY_UP;

sgl_int_t _term_key_new(unsigned char in[]) {
  sgl_int_t k = 0;
  for (unsigned char *c = in, *p = (void *)&k; *c; c++, p++) { *p = *c; }
  return k;
}

void term_init_keys() {
  TERM_KEY_BSPACE = term_key_new(127);
  TERM_KEY_DOWN = term_key_new(27, 91, 66);
  TERM_KEY_ESC = term_key_new(27);
  TERM_KEY_LEFT = term_key_new(27, 91, 68);
  TERM_KEY_RIGHT = term_key_new(27, 91, 67);
  TERM_KEY_RETURN = term_key_new(10);
  TERM_KEY_UP = term_key_new(27, 91, 65);

  TERM_KEY_F1 = term_key_new(27, 79, 80);
  TERM_KEY_F2 = term_key_new(27, 79, 81);
  TERM_KEY_F3 = term_key_new(27, 79, 82);
  TERM_KEY_F4 = term_key_new(27, 79, 83);
  TERM_KEY_F5 = term_key_new(27, 91, 49, 53, 126);
  TERM_KEY_F6 = term_key_new(27, 91, 49, 55, 126);
  TERM_KEY_F7 = term_key_new(27, 91, 49, 56, 126);
  TERM_KEY_F8 = term_key_new(27, 91, 49, 57, 126);
  TERM_KEY_F9 = term_key_new(27, 91, 50, 48, 126);
  TERM_KEY_F10 = term_key_new(27, 91, 50, 49, 126);
}
