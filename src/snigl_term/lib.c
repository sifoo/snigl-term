#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include "snigl/io.h"
#include "snigl/sgl.h"
#include "snigl/str.h"

#include "snigl_term/key.h"
#include "snigl_term/lib.h"
#include "snigl_term/types/key.h"

static void move_down(sgl_int_t n, struct sgl_buf *out) {
  char esc[] = {27, '[', 0};
  sgl_buf_printf(out, "%s%" SGL_INT "B", esc, n);
}

static void move_left(sgl_int_t n, struct sgl_buf *out) {
  char esc[] = {27, '[', 0};
  sgl_buf_printf(out, "%s%" SGL_INT "D", esc, n);
}

static bool raw_mode_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {

  struct termios ios;
   
  if (tcgetattr(STDIN_FILENO, &ios) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed reading term settings: %d", errno));

    return false;
  }

  ios.c_lflag &= ~ICANON;
  ios.c_lflag &= ~ECHO;

  if (tcsetattr(STDIN_FILENO, TCSANOW, &ios) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed writing term settings: %d", errno));
    return false;
  }

  int ff = 0;
  
  if ((ff = fcntl(STDIN_FILENO, F_GETFL, 0)) < 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed reading file flags: %d", errno));

    return false;
  }
   
  if (fcntl(STDIN_FILENO, F_SETFL, ff | O_NONBLOCK) < 0) { 
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed writing file flags: %d", errno));

    return false;
  }

  if (setvbuf(stdin, NULL, _IONBF, 0) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed disabling input buffering: %d", errno));

    return false;
  }

  if (setvbuf(stdout, NULL, _IONBF, 0) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed disabling output buffering: %d", errno));

    return false;
  }

  return true;
}

static bool normal_mode_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct termios ios;
   
  if (tcgetattr(STDIN_FILENO, &ios) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed reading term settings: %d", errno));

    return false;
  }

  ios.c_lflag |= ICANON;
  ios.c_lflag |= ECHO;

  if (tcsetattr(STDIN_FILENO, TCSANOW, &ios) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed writing term settings: %d", errno));

    return false;
  }

  int ff = 0;
  
  if ((ff = fcntl(STDIN_FILENO, F_GETFL, 0)) < 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed reading file flags: %d", errno));

    return false;
  }
   
  if (fcntl(STDIN_FILENO, F_SETFL, ff & ~O_NONBLOCK) < 0) { 
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed writing file flags: %d", errno));

    return false;
  }

  if (setvbuf(stdin, NULL, _IOFBF, BUFSIZ) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed restoring input buffering: %d", errno));

    return false;
  }

  if (setvbuf(stdout, NULL, _IOFBF, BUFSIZ) != 0) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Failed restoring output buffering: %d", errno));

    return false;
  }

  return true;
}

static bool term_size_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct winsize s;
  
  if (ioctl(STDIN_FILENO, TIOCGWINSZ, &s) != 0) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Failed to get term size: %d", errno));
    return false;
  }

  sgl_xy_init(&sgl_push(sgl, sgl->XY)->as_xy, s.ws_col, s.ws_row);
  return true;
}

static void move(sgl_int_t x, sgl_int_t y, struct sgl_buf *out) {
  char esc[] = {27, '[', 0};
  sgl_buf_printf(out, "%s%" SGL_INT ";%" SGL_INT "H", esc, y + 1, x + 1);
}

static bool move_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *pv = sgl_pop(sgl);
  struct sgl_xy *p = &pv->as_xy;
  move(p->x, p->y, sgl_stdout(sgl));
  sgl_val_free(pv, sgl);
  return true;
}

static bool move_xy_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *y = sgl_pop(sgl), *x = sgl_pop(sgl);
  move(x->as_int, y->as_int, sgl_stdout(sgl));
  sgl_val_free(y, sgl);
  sgl_val_free(x, sgl);
  return true;
}

static bool clear_term_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  char esc[] = {27, '[', '2', 'J', 0};
  sgl_buf_putcs(sgl_stdout(sgl), esc);
  return true;
}

static bool reset_term_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  char esc[] = {27, '[', '0', 'm', 0};
  sgl_buf_putcs(sgl_stdout(sgl), esc);
  return true;
}

static bool save_pos_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  char esc[] = {27, '[', 's', 0};
  sgl_buf_putcs(sgl_stdout(sgl), esc);
  return true;
}

static bool load_pos_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  char esc[] = {27, '[', 'u', 0};
  sgl_buf_putcs(sgl_stdout(sgl), esc);
  return true;
}

static bool pick_bcolor_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *cv = sgl_pop(sgl);
  struct sgl_rgb *c = &cv->as_rgb;
  char esc[] = {27, '[', 0};
  
  sgl_buf_printf(sgl_stdout(sgl),
                 "%s48;2;%" SGL_INT ";%" SGL_INT ";%" SGL_INT "m",
                 esc, c->r, c->g, c->b);

  sgl_val_free(cv, sgl);
  return true;
}

static bool pick_fcolor_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *cv = sgl_pop(sgl);
  struct sgl_rgb *c = &cv->as_rgb;
  char esc[] = {27, '[', 0};
  
  sgl_buf_printf(sgl_stdout(sgl),
                 "%s38;2;%" SGL_INT ";%" SGL_INT ";%" SGL_INT "m",
                 esc, c->r, c->g, c->b);

  sgl_val_free(cv, sgl);
  return true;
}

static bool underline_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *on = sgl_pop(sgl);
  char esc[] = {27, '[', 0};
  sgl_buf_printf(sgl_stdout(sgl), "%s%sm", esc, on->as_bool ? "4" : "24");
  sgl_val_free(on, sgl);
  return true;
}

static bool fill_row_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *c = sgl_pop(sgl), *n = sgl_pop(sgl);
  struct sgl_buf *out = sgl_stdout(sgl);
  sgl_buf_grow(out, out->len + n->as_int);
  memset(out->data + out->len, c->as_char, n->as_int);
  out->len += n->as_int;
  sgl_val_free(c, sgl);
  sgl_val_free(n, sgl);
  return true;
}

static bool fill_rect_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *c = sgl_pop(sgl), *r = sgl_pop(sgl);
  struct sgl_xy *rxy = &r->as_xy;
  struct sgl_buf *out = sgl_stdout(sgl);

  for (sgl_int_t i = 0; i < rxy->y; i++) {
    if (i > 0) {
      move_left(rxy->x, out);
      move_down(1, out);
    }
    
    sgl_buf_grow(out, out->len + rxy->x);
    memset(out->data + out->len, c->as_char, rxy->x);
    out->len += rxy->x;
  }
  
  sgl_val_free(c, sgl);
  sgl_val_free(r, sgl);
  return true;
}

static char *get_key_io_imp(struct sgl *sgl,
                            struct sgl_val *ms,
                            struct sgl_type *key_type) {
  struct pollfd pfd = {0};
  pfd.fd = STDIN_FILENO;
  pfd.events = POLLIN;
  int ok = poll(&pfd, 1, ms->as_int);

  if (ok != 1) {
    ms->type = sgl->Nil;
    return NULL;
  }
  
  sgl_int_t k = 0;
  unsigned char c = 0, *p = (void *)&k;
  int n = 0;
  
  while ((n = read(STDIN_FILENO, &c, 1)) == 1) {
    //printf("%d\n", c);
    *p++ = c;
  }

  if (n == -1 && errno != EAGAIN && errno != EWOULDBLOCK) {
    return sgl_sprintf("Failed reading key: %d", errno);
  }

  if (k)  {
    ms->type = key_type;
    ms->as_int = k;
  } else {
    ms->type = sgl->Nil;
  }
  
  return NULL;
}

#ifdef SGL_USE_ASYNC

static char *get_key_io_run(struct sgl_io *io,
                            struct sgl *sgl,
                            struct sgl_io_thread *thread) {
  return get_key_io_imp(sgl, io->args[0], io->args[1]);
}

static void get_key_io_free(struct sgl_io *io, struct sgl *sgl) {
  struct sgl_val *out = io->args[0];
  
  if (io->error) {
    sgl_val_free(out, sgl);    
    sgl_error(sgl, &io->pos, io->error);
  } else {
    sgl_ls_push(sgl_stack(sgl), &out->ls);
  }
  
  sgl_io_free(io, sgl);
}

#endif

static bool get_key_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct term_lib *tl = sgl_baseof(struct term_lib, lib, fimp->func->def.lib);
  struct sgl_val *ms = sgl_pop(sgl);
  
#ifdef SGL_USE_ASYNC
  if (sgl_is_async(sgl)) {
    struct sgl_io *io = sgl_io_new(sgl, -1, ms, tl->Key);
    io->run = get_key_io_run;
    io->free = get_key_io_free;
    *rpc = sgl_io_yield(io, sgl, sgl_io_thread(sgl))->next;
    return true;
  }
#endif

  char *e = get_key_io_imp(sgl, ms, tl->Key);

  if (e) {
    sgl_error(sgl, sgl_pos(sgl), e);
    sgl_val_free(ms, sgl);
    return false;
  }

  sgl_ls_push(sgl_stack(sgl), &ms->ls);
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  struct term_lib *tl = sgl_baseof(struct term_lib, lib, l);

  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "Int"),
               sgl_sym(sgl, "Nil"))) {
    return false;
  }

  tl->Key = term_key_type_new(sgl, pos, l, sgl_sym(sgl, "Key"),
                              sgl_types(sgl->T));
  
  sgl_val_init(&tl->k_bspace, sgl, tl->Key, NULL)->as_int = TERM_KEY_BSPACE;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-bspace"), &tl->k_bspace);

  sgl_val_init(&tl->k_down, sgl, tl->Key, NULL)->as_int = TERM_KEY_DOWN;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-down"), &tl->k_down);

  sgl_val_init(&tl->k_esc, sgl, tl->Key, NULL)->as_int = TERM_KEY_ESC;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-esc"), &tl->k_esc);

  sgl_val_init(&tl->k_f1, sgl, tl->Key, NULL)->as_int = TERM_KEY_F1;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-f1"), &tl->k_f1);

  sgl_val_init(&tl->k_f2, sgl, tl->Key, NULL)->as_int = TERM_KEY_F2;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-f2"), &tl->k_f2);

  sgl_val_init(&tl->k_left, sgl, tl->Key, NULL)->as_int = TERM_KEY_LEFT;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-left"), &tl->k_left);

  sgl_val_init(&tl->k_return, sgl, tl->Key, NULL)->as_int = TERM_KEY_RETURN;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-return"), &tl->k_return);

  sgl_val_init(&tl->k_right, sgl, tl->Key, NULL)->as_int = TERM_KEY_RIGHT;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-right"), &tl->k_right);

  sgl_val_init(&tl->k_up, sgl, tl->Key, NULL)->as_int = TERM_KEY_UP;
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "k-up"), &tl->k_up);

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "raw-mode"),
                    raw_mode_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "normal-mode"),
                    normal_mode_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "term-size"),
                    term_size_imp,
                    sgl_rets(sgl->XY)); 
  
  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "move"),
                    move_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->XY)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "move-xy"),
                    move_xy_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "clear-term"),
                    clear_term_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "reset-term"),
                    reset_term_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "save-pos"),
                    save_pos_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "load-pos"),
                    load_pos_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "pick-bcolor"),
                    pick_bcolor_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->RGB)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "pick-fcolor"),
                    pick_fcolor_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->RGB)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "underline"),
                    underline_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Bool)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fill-row"),
                    fill_row_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Char)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fill-rect"),
                    fill_rect_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->XY), sgl_arg(sgl->Char)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "get-key"),
                    get_key_imp,
                    sgl_rets(sgl_type_union(sgl, pos, tl->Key, sgl->Nil)),
                    sgl_arg(sgl_type_union(sgl, pos, sgl->Int, sgl->Nil))); 

  return true;
}

static void lib_deinit(struct sgl_lib *l, struct sgl *sgl) {
  struct term_lib *tl = sgl_baseof(struct term_lib, lib, l);
  free(tl);
}

struct sgl_lib *term_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct term_lib *tl = malloc(sizeof(struct term_lib));
  struct sgl_lib *l = &tl->lib;
  sgl_lib_init(l, sgl, pos, sgl_sym(sgl, "term"));
  l->init = lib_init;
  l->deinit = lib_deinit;
  return l;
}

bool snigl_term_init(struct sgl *sgl, struct sgl_pos *pos) {
  term_init_keys();
  return sgl_add_lib(sgl, pos, term_lib_new(sgl, pos));
}
